var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* callback endpoint */
var callback = require('./api/callback');
router.use('/api/callback', callback);


module.exports = router;
