var express = require('express');
var router = express.Router();
var request = require('request');

var Logger = require('../../lib/logger');
var ChatClient = require('../../lib/ChatClient');
var LineClient = require('../../lib/LineClient');

// LINE BOT callback request endpoint.
router.post('/', function(req, res, next) {
  var log = new Logger();
  log.info('BOT callback request start.');
  log.info('request: ' + JSON.stringify(req.body));

  // TODO 複数メッセージの対応
  var received = req.body.result[0];

  var chatClient = new ChatClient(log);
  chatClient.talk(received.content.text)
    .then(function(chatRes) {
      var toList = [received.content.from];
      var lineClient = new LineClient(log);
      return lineClient.send(toList, chatRes.utt);

    })
    .then(function() {
      res.status(201).send();
    })
    .catch(function(err) {
      next(err);
    });

});

module.exports = router;
