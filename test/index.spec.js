var request = require('supertest');
var express = require('express');

var app = require('../app');

describe('/', () => {
  it('should return home page.', (done) => {
    request(app)
      .get('/')
      .expect('Content-Type', /text\/html/)
      .expect(200, done);
  });
});
