var request = require('supertest');
var express = require('express');
var nock = require('nock');
var expect = require('chai').expect;

var app = require('../../app');

describe('/callback', () => {
  var endpoint = '/api/callback';

  afterEach(() => {
    nock.cleanAll();
  });

  it('should accept POST.', (done) => {

    // mock DOCOMO API endpoint
    nock('https://api.apigw.smt.docomo.ne.jp')
      .post('/dialogue/v1/dialogue')
      .query(true)
      .reply(200, {
        "utt": "今日は、何の日でしょうね。",
        "yomi": "今日は、何の日でしょうね。",
        "mode": "dialog",
        "da": "3",
        "context": "おはよう"
      });

    // mock LINE BOT API endpoint
    nock('https://trialbot-api.line.me')
      .post('/v1/events')
      .reply(200);

    request(app)
      .post(endpoint)
      .send({
        "result":[
          {
            "from":"u206d25c2ea6bd87c17655609a1c37cb8",
            "fromChannel":"1341301815",
            "to":["u0cc15697597f61dd8b01cea8b027050e"],
            "toChannel":"1441301333",
            "eventType":"138311609000106303",
            "id":"ABCDEF-12345678901",
            "content":{
              "text": "hello!!"
            }
          }
        ]
      })
      .expect(201, done);
  });

  it('should respond error message when failed to respond.', (done) => {

    // mock DOCOMO API endpoint
    nock('https://api.apigw.smt.docomo.ne.jp')
      .post('/dialogue/v1/dialogue')
      .query(true)
      .reply(200, {
        "utt": "今日は、何の日でしょうね。",
        "yomi": "今日は、何の日でしょうね。",
        "mode": "dialog",
        "da": "3",
        "context": "おはよう"
      });

    // mock LINE BOT API endpoint
    nock('https://trialbot-api.line.me')
      .post('/v1/events')
      .reply(400);

    request(app)
      .post(endpoint)
      .send({
        "result":[
          {
            "from":"u206d25c2ea6bd87c17655609a1c37cb8",
            "fromChannel":"1341301815",
            "to":["u0cc15697597f61dd8b01cea8b027050e"],
            "toChannel":"1441301333",
            "eventType":"138311609000106303",
            "id":"ABCDEF-12345678901",
            "content":{
            }
          }
        ]
      })
      .expect('Content-Type', /json/)
      .expect(400)
      .end((err, res)=> {
        expect(res.body.message).to.be.a.String;
        done();
      });
  });

  it('should not accept GET.', (done) => {
    request(app)
      .get(endpoint)
      .expect('Content-Type', /json/)
      .expect(404, done);
  });

});
