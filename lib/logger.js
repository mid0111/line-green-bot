
var Logger = function() {
};

Logger.prototype.info = (str) => {
  console.log('[INFO] ' + str);
};

Logger.prototype.warn = (str) => {
  console.log('[WARN] ' + str);
};

Logger.prototype.error = (str, e) => {
  console.log('[ERROR] ' + str);
  console.log(e.stack);
};

module.exports = Logger;

