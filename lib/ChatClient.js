/**
 * Chat client class.
 * @name ChatClient.js
 */
'use strict';

var request = require('request');

// DOCOMO API settings
var TARGET_URL = 'https://api.apigw.smt.docomo.ne.jp/dialogue/v1/dialogue?APIKEY=' + process.env.DOCOMO_API_KEY;

function ChatClient(log) {
  this.log = log;
}

ChatClient.prototype.talk = function(sendText) {
  return new Promise(function(resolve, reject) {
    var requestBody = {
      utt: sendText,
      mode: 'dialog'
    };

    this.log.info('Call docomo dialogue API start.');
    request
      .post({
        url: TARGET_URL,
        json: requestBody
      }, function(err, sendMsgRes) {
        if(err) {
          this.log.error('Failed to call docomo dialogue API.', err);
          reject(err);
          return;
        }
        this.log.info('Call docomo dialogue API success.');
        resolve(sendMsgRes.body);
        return;
      }.bind(this));

  }.bind(this));
};

module.exports = ChatClient;
