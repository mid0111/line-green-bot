'use strict';

/**
 *
 * LINE BOT API client class.
 *
 * @name lineClient.js
 */

var request = require('request');

// LINE BOT API settings
var SEND_MESSAGE_URL = 'https://trialbot-api.line.me/v1/events';
var SEND_MESSAGE_EVENT_TYPE = '138311608800106203';
var toChannel = 1383378250;

/**
 * constructor
 * @param {Object} log logger
 */
function LineClient(_log) {
  this.log = _log;
}

/**
 * send
 * @param {Array} toList list of target
 * @param {String} text message to send
 * @return {Promise}
 */
LineClient.prototype.send = function(toList, text) {

  return new Promise(function(resolve, reject) {

    var sendInformation = {
      to: toList,
      toChannel: toChannel,
      eventType: SEND_MESSAGE_EVENT_TYPE,
      content: {
        contentType: 1,
        toType: 1,
        text: text
      }
    };

    this.log.info('Call LINE BOT API start.');
    request
      .defaults({proxy: process.env.FIXIE_URL})
      .post({
        url: SEND_MESSAGE_URL,
        headers: {
          'X-Line-ChannelID': process.env.CHANNEL_ID,
          'X-Line-ChannelSecret': process.env.CHANNEL_SECRET,
          'X-Line-Trusted-User-With-ACL': process.env.CHANNEL_MID
        },
        json: sendInformation
      }, function(err, sendMsgRes) {
        if(err) {
          this.log.error('Failed to call LINE BOT API.', err);
          reject(err);
          return;
        }
        this.log.info('Call LINE BOT API success.');
        resolve(sendMsgRes);
        return;
      }.bind(this));

  }.bind(this));

};

module.exports = LineClient;
